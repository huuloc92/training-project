global class StatisticsPickList extends VisualEditor.DynamicPickList {
	global override VisualEditor.DataRow getDefaultValue(){
		VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('', '');
		return defaultValue;
	}

	global override VisualEditor.DynamicPickListRows getValues() {
		
		VisualEditor.DynamicPickListRows  valueList = new VisualEditor.DynamicPickListRows();
		List<Indicatior_Item__mdt> listItem = [SELECT MasterLabel,
													Object_Name__c
												FROM Indicatior_Item__mdt];
		for (Indicatior_Item__mdt item: listItem) {
			VisualEditor.DataRow value = 
						new VisualEditor.DataRow(item.MasterLabel, item.Object_Name__c);
			valueList.addRow(value);
		}
		return valueList;
	}

}