public with sharing class StatisticsController {
	
	@AuraEnabled
	public static Decimal getStatistics(Id objId, String objName, String strType) {
		try {
			Decimal result = 0;
			if (strType == 'Case') { // ACCOUNT & ASSET - CASE
				String query = 'SELECT COUNT() FROM Case WHERE ' 
								+ objName + 'Id = :objId '
								+'AND Status != \'Closed\'';
				result = Database.countQuery(query);
				return result;
			} else if (strType == 'WorkOrder') { // ACCOUNT & ASSET - WO
				String query = 'SELECT COUNT() FROM WorkOrder WHERE ' 
								+ objName + 'Id = :objId '
								+'AND Status != \'Work Order Closed\'';
				result = Database.countQuery(query);
				return result;
			} else if (strType == 'PartsRequest') {
				if (objName == 'Account') { // ACCOUNT - PR
					Map<Id, WorkOrder> woMap = 
								new Map<Id, WorkOrder>([SELECT Id 
														FROM WorkOrder
														WHERE AccountId = :objId]);
					if (!woMap.isEmpty()) {
						Set<String> statusSet = new Set<String>{'Submitted','Shipped','Pending'};
						result = [SELECT COUNT() 
									FROM Parts_Request__c
									WHERE Work_Order__c IN : woMap.keySet()
									AND Status__c IN :statusSet];
						return result;
					}
				} else if (objName == 'Asset') { // ASSET - PR
					Set<String> statusSet = new Set<String>{'Submitted','Shipped','Pending'};
					result = [SELECT COUNT() 
								FROM Parts_Request__c
								WHERE Asset__c = :objId
								AND Status__c IN :statusSet];
					return result;
				}
			} else if (strType == 'BillBack') {
				Map<Id, Case> caseMap = new Map<Id, Case>();
				if (objName == 'Account') {// ACCOUNT - BB
					caseMap = new Map<Id, Case>([SELECT Id
												FROM Case
												WHERE AccountId = :objId]);
				} else if (objName == 'Asset') { // ASSET - BB
					caseMap = new Map<Id, Case>([SELECT Id
												FROM Case
												WHERE AssetId = :objId]);
				}

				if (!caseMap.isEmpty()) {
					Set<String> statusSet = new Set<String>{'Approved','Partially Approved'};
					List<AggregateResult> resultLst = [SELECT SUM(Total_Bill_Back_Cost__c) sum
														FROM Bill_back__c
														WHERE Case__c IN : caseMap.keySet()
														AND Status__c IN :statusSet];
					if (resultLst != null && !resultLst.isEmpty()) {
						String strResult = '' + resultLst[0].get('sum') ;
						result = Decimal.ValueOf(strResult) ;
						return result;	
					}
				}
			}
			return null;
		} catch (Exception ex) {
			System.debug('@@@@ex '+ ex);
			throw new AuraHandledException(ex.getMessage());
		}
	}
}