public class AddPrimaryContact implements Queueable  {

	private Contact cont;
	private String state;

	public AddPrimaryContact (Contact objCont, String strState) {
		this.cont = objCont;
		this.state = strState;
	}

    public void execute(QueueableContext context) {
        List<Account> lstAcc = [SELECT Id FROM Account WHERE BillingState = :state LIMIT 200];
        if (lstAcc != null && cont != null) {
        	List<Contact> lstCont = new List<Contact>();
        	for (Account acc : lstAcc) {
        		Contact newCon = cont.clone();
        		newCon.AccountId = acc.Id;
        		lstCont.add(newCon);
        	}

        	insert lstCont;
        }
    }

}