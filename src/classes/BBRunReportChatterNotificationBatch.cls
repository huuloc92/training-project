/*************************************************
* Class Name: BBRunReportChatterNotificationBatch 
* Author: Thom Phan
* Date: 13-Jun-2018
* Requirement/Project Name: Vantage
* Requirement/Project 
* 	The "Did you run the Determination Report?" checkbox must be selected within 20 days of the Compliance Type Date.
* If the Compliance Type Date was 15 days ago, the Compliance Type is anything other than "No further action", 
* and at least one of the WOLI Determination Report checkboxes is not checked, send Chatter notification to the WO owner. 
* Notification: You have 5 days to run the Determination Report and check the box for the WOLIs on BB #." Need to show a link to the BB.
*************************************************/
public class BBRunReportChatterNotificationBatch implements Database.Batchable<sObject>, Database.Stateful {
	public BBRunReportChatterNotificationBatch() {
	}

	public List<Bill_Back__c> start (database.BatchableContext BC){
		Set<String> hbgUserProfile  = new Set<String>{'Home Building Facility', 'HBF Service Technician'};
		
		return [SELECT Id, OwnerId, 
						Name
				FROM Bill_Back__c 
				WHERE CreatedDate < LAST_N_DAYS:25 
						AND Owner.Profile.Name IN :hbgUserProfile
               			AND Status__c ='Submitted'];
	}
	
	public void execute(Database.BatchableContext BC, List<Bill_Back__c> bbList){
		System.debug('############bbList: '+bbList);
		List<Id> bbIDs = new List<ID>();
		if(bbList.size() > 0){
			for(Bill_Back__c bb: bbList){
				bbIDs.add(bb.ID);
			}

			Map<ID, List<WorkOrderLineItem>> workOrderLineItemMap = new Map<ID, List<WorkOrderLineItem>>();
			for(List<WorkOrderLineItem> wolis : [Select Id,
														Bill_Back__c,
														Compliance_Type_Date__c,
														Compliance_Type__c,
														Determination_Report__c
												FROM WorkOrderLineItem 
												WHERE Bill_Back__c  IN :bbIDs]){
				for(WorkOrderLineItem woli: wolis){
					if(workOrderLineItemMap.get(woli.Bill_Back__c) == null){
						List<WorkOrderLineItem> temp = new List<WorkOrderLineItem>();
						temp.add(woli);
						workOrderLineItemMap.put(woli.Bill_Back__c, temp);
					}else{
						workOrderLineItemMap.get(woli.Bill_Back__c).add(woli);
					}
				}
			}

			List<Bill_Back__c> bbNeedToSendChatterNotification = new List<Bill_Back__c>();

			for(Bill_Back__c bb: bbList){
				if(workOrderLineItemMap.get(bb.ID) != null){
					Boolean hasWOLIInNotCondition = false;
					for(WorkOrderLineItem woli: workOrderLineItemMap.get(bb.ID)){
						
						if(woli.Compliance_Type_Date__c != null && (woli.Compliance_Type_Date__c + 14 <= System.today()) && woli.Compliance_Type__c != 'No Further Action'
							&& !woli.Determination_Report__c){                            
							hasWOLIInNotCondition = true;
						}
					}

					if(hasWOLIInNotCondition){
						bbNeedToSendChatterNotification.add(bb);
					}
				}
			}
			
			System.debug('############bbNeedToSendChatterNotification: '+bbNeedToSendChatterNotification);

			if(bbNeedToSendChatterNotification.size() >0){
				for(Bill_Back__c bb: bbNeedToSendChatterNotification){
					String message = 'You have 5 days to run the Determination Report and check the box for the WOLIs on ';
					new CreateChatterNotification(bb.OwnerId, bb.ID, bb.Name, message);
				}
			}            
		}        
	}

	public void finish(Database.BatchableContext BC){        
		AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed,TotalJobItems,CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
		Messaging.SingleEmailMessage batchEmail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] {a.CreatedBy.Email};
		batchEmail.setToAddresses(toAddresses);
		batchEmail.setSubject('Chatter notification status: ' + a.Status);
		batchEmail.setPlainTextBody('Chatter notification with '+ a.NumberOfErrors + ' failures.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {batchEmail});
	}
}