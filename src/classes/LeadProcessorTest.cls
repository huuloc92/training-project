@isTest
private class LeadProcessorTest {
	static {
		List<Lead> lstLead = new List<Lead>();
		for (Integer i = 0; i < 200; i++) {
			Lead objLead = new Lead();
			objLead.FirstName = 'FName';
			objLead.LastName = 'LName';
			objLead.Company = 'CapGemini';
			lstLead.add(objLead);
		}
		insert lstLead;
	}

    static testMethod void testBatch() {
        Test.startTest();
        	LeadProcessor objBatch = new LeadProcessor();
        	Database.executeBatch(objBatch);
        Test.stopTest();
    }
}