public with sharing class lookUpController {
	@AuraEnabled
	public static List<Account> fetchAccount(String searchKeyWord) {
		String searchKey = searchKeyWord + '%';
		List<Account> returnList = new List <Account> ();
		//List<Account> lstOfAccount = [select id, Name from Account where Name LIKE: searchKey];

		String searchString = 'FIND \'' + searchKeyWord + '\' IN Name RETURNING Account (id, name)';

		List<List<SObject>> listResult = Search.query(searchString);

		returnList = (List<Account>) listResult.get(0);

		return returnList;
	}
}