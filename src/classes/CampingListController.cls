public with sharing class CampingListController {
	public CampingListController() {
		
	}

	@AuraEnabled
	public static Camping_Item__c saveItem(Camping_Item__c objCampingItem){
		upsert objCampingItem;
		Camping_Item__c objItem = [SELECT Id,
											Name,
											Price__c,
											Quantity__c,
											Packed__c,
											Account__c,
											Account__r.Name
									FROM Camping_Item__c
									WHERE Id = :objCampingItem.Id];
		return objItem;
	}

	@AuraEnabled
	public static List<Camping_Item__c> getItems() {
		List<Camping_Item__c> listCampingItem = [SELECT Id,
														Name,
														Price__c,
														Quantity__c,
														Packed__c,
														Account__c,
														Account__r.Name
												FROM Camping_Item__c
												ORDER BY CreatedDate DESC];
		return listCampingItem;
	}
}