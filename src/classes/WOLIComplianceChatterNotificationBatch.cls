/*************************************************
* Class Name: WOLIComplianceChatterNotificationBatch
* Author: Thom Phan
* Date: 24-May-2018
* Requirement/Project Name: Vantage
* Requirement/Project 
* The "Compliance Type" field must be populated within 30 days of the WO being created. 
* If the Work Order was created 25 days ago and at least one of the WOLI Compliance Type is none, send Chatter notification to the WO owner. 
* Notification: "You have 5 days to enter the Compliance Type for the WOLIs on WO #". Need to show a link to the WO
*************************************************/
public class WOLIComplianceChatterNotificationBatch implements Database.Batchable<sObject>, Database.Stateful {

    //public string query;
    
    public WOLIComplianceChatterNotificationBatch(){
        //this.query = 'SELECT Id, ownerID, WorkOrderNumber, Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c FROM WorkOrder WHERE CreatedDate < LAST_N_DAYS:25 AND Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c != NULL';
    }
    
    //public Database.QueryLocator start (database.BatchableContext BC){
    //    return Database.getQueryLocator(query);
    //}

    public List<WorkOrder> start (database.BatchableContext BC){
        Set<String> hbgUserProfile  = new Set<String>{'Home Building Facility', 'HBF Service Technician','System Administrator'};
        
        return [SELECT Id, OwnerId, 
                        WorkOrderNumber, 
                        Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c 
                FROM WorkOrder 
                WHERE CreatedDate < LAST_N_DAYS:25 
                        AND Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c != NULL 
                        AND Owner.Profile.Name IN :hbgUserProfile];
    }
    
    public void execute(Database.BatchableContext BC, List<WorkOrder> woList){
        System.debug('############woList: '+woList);
        List<Id> woIDs = new List<ID>();
        if(woList.size() > 0){            
            for(WorkOrder wo: woList){
                woIDs.add(wo.ID);
            }

            Map<ID, List<WorkOrderLineItem>> workOrderLineItemMap = new Map<ID, List<WorkOrderLineItem>>();
            for(List<WorkOrderLineItem> wolis : [Select Id, WorkOrderID, Compliance_Type__c FROM WorkOrderLineItem WHERE WorkOrderID  in: woIDs]){
                for(WorkOrderLineItem woli: wolis){
                    if(workOrderLineItemMap.get(woli.WorkOrderID) == null){
                        List<WorkOrderLineItem> temp = new List<WorkOrderLineItem>();
                        temp.add(woli);
                        workOrderLineItemMap.put(woli.WorkOrderID, temp);
                    }else{
                        workOrderLineItemMap.get(woli.WorkOrderID).add(woli);
                    }
                }
            }

            List<WorkOrder> woNeedToSendChatterNotification = new List<WorkOrder>();

            for(WorkOrder wo: woList){
                if(workOrderLineItemMap.get(wo.ID) != null){
                    Boolean hasWOLINoneComplianceType = false;
                    for(WorkOrderLineItem woli: workOrderLineItemMap.get(wo.ID)){
                        if(woli.Compliance_Type__c == null){
                            hasWOLINoneComplianceType = true;
                        }
                    }

                    if(hasWOLINoneComplianceType){
                        woNeedToSendChatterNotification.add(wo);
                    }
                }
            }
            
            System.debug('############woNeedToSendChatterNotification: '+woNeedToSendChatterNotification);

            if(woNeedToSendChatterNotification.size() >0){
                for(WorkOrder wo: woNeedToSendChatterNotification){
                    //String message = 'You have 5 days to run the Determination Report and check the box for the WOLIs on WO';
                    String message = 'You have 5 days to enter the Compliance Type for the WOLIs on ';
                    new CreateChatterNotification(wo.Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c, wo.ID, wo.WorkOrderNumber, message);
                }
            }            
        }        
    }

    public void finish(Database.BatchableContext BC){        
        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed,TotalJobItems,CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
        Messaging.SingleEmailMessage batchEmail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        batchEmail.setToAddresses(toAddresses);
        batchEmail.setSubject('Chatter notification status: ' + a.Status);
        batchEmail.setPlainTextBody('Chatter notification with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {batchEmail});
    }

}