public with sharing class TestCtrl {
	public TestCtrl() {
		
	}

	@AuraEnabled
	public static String tempAction() {
		Long timeDiff = 0;
		DateTime firstTime = System.now();
		do
		{
			timeDiff = System.now().getTime() - firstTime.getTime();
		}
		while(timeDiff <= 5000);
		return 'abc';
	}

	@AuraEnabled
	public static void tempThrowAction() {
		throw new MyException();
	}

	public class MyException extends Exception {

	}
}