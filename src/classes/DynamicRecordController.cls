/*************************************************
 * Class Name: DynamicRecordController 
 * Author: Thom Phan
 * Date: 23-Apr-2018
 * Description: Handler DynamicRecordController. 
 * Revision: Thom Phan
 *************************************************/
public with sharing class DynamicRecordController {
    @AuraEnabled
    public static List<WorkOrderLineItem> getItemsList(String objApiName, String parentId, List<String> fieldNames, String parentFieldName) { 
        System.debug('########objApiName: '+objApiName+', parentId: '+parentId+', fieldNames: '+fieldNames); 
    	try{
            if(objApiName != null && parentId != null){                

                List<String> fields = new List<String>();
                for(String fieldVal : fieldNames){                    
                    if(fieldVal.split('\\+')[0] != null){                    
                        fields.add(fieldVal.split('\\+')[0]);
                    }
                }
                return selectSObjectsByField(objApiName, fields, parentFieldName,  parentId);
            }
            return null;
        }catch(Exception ex){
            System.debug('@@@@ex '+ ex);
            throw new AuraHandledException(ex.getMessage());  
        }
    }

    public static List<SObject> selectSObjectsByField(String sObjectName, List<String> fields, String filterFieldName, String filterFieldId){
        List<SObject> sObjects = new List<SObject>();
        String querySelect = 'SELECT '+String.join(fields, ',')+' FROM ' + sObjectName + ' WHERE '+ filterFieldName +' = \'' + filterFieldId + '\' order by '+fields[0]+' asc';        
    
        System.debug('!!!!!!querySelect: '+querySelect);
        for(List<SObject> sOs : Database.query(querySelect)){
            sObjects.addAll(sOs);
        }
        return sObjects;
    }


    @AuraEnabled
    public static List<SObject> saveItems(List<SObject> items, String objApiName, 
                                                        String parentId, List<String> fieldNames, 
                                                        String parentFieldName){
    	System.debug('######saveItems: '+items+', parentFieldName: '+parentFieldName+', objApiName: '+objApiName);
        try{
        	if(items != null){
        		List<SObject> insertList = new List<SObject>();
                List<SObject> updateList = new List<SObject>();

                for(SObject woli : items){
                    if(woli.id != null){
                        updateList.add(woli);
                    }else{
                        insertList.add(woli);
                    }
                }
                // There is an error that can not upsert sObject, so we need to seperate it here.
                if(updateList != null){
                    System.debug('@@@@updateList '+ updateList);
                    update updateList;
                }
                
                if(insertList != null){
                    System.debug('@@@@insertList '+ insertList);

                    // Handle error when insert WOLI linked with BB, we need to find WO id to link to WOLI
                    if(objApiName == 'WorkOrderLineItem' && parentFieldName == 'Bill_Back__c'){
                        List<WorkOrderLineItem> wolis = new List<WorkOrderLineItem>();
                        Bill_Back__c bb = [SELECT Work_Order__c FROM Bill_Back__c where id =: parentId];
                        for(SObject o: insertList){
                            WorkOrderLineItem woli = (WorkOrderLineItem)o;
                            woli.WorkOrderID = bb.Work_Order__c;
                            wolis.add(woli);
                        }
                        System.debug('#######wolis: '+wolis);
                        insert wolis;
                    }else{
                        insert insertList;
                    }
                }

                // Get list again to get WOLI number and render list again
                items = getItemsList(objApiName, parentId, fieldNames, parentFieldName);
        	}

            return items;
        }catch(Exception ex){
        	String err = ex.getMessage();
            if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                List<String> tempErr =  err.split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ');
                err = tempErr[1];
            }
            throw new AuraHandledException(err);  
        }
    }

    @AuraEnabled
    public static void deleteItem(String valueId, String objApiName){
        System.debug('#######deleteItem: '+valueId);
        try{            
            List<SObject> delList = selectSObjectsByField(objApiName, new List<String>{'ID'}, 'ID', valueId);
            if(!delList.isEmpty()){
                delete delList;
            }
        }catch(Exception ex){
            System.debug('@@@@ex '+ ex);
            throw new AuraHandledException(ex.getMessage());  
        }
    }

    @AuraEnabled
    public static List<SObject> eAction(List<SObject> items, String objApiName, String actionVal){
        system.debug('##########objApiName:'+objApiName+', actionVal: '+actionVal+', items: '+items);
        try{            
            if(objApiName == 'WorkOrderLineItem'){
                return updateWOLIStatus(items, actionVal);
            }else if(objApiName == 'Parts_Request_Line_Item__c'){
                return updatePRLIStatus(items, actionVal);
            }
            return null;
        }catch(Exception ex){
            System.debug('@@@@ex '+ ex);
            String err = ex.getMessage();
            if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                List<String> tempErr =  err.split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ');
                err = tempErr[1];
            }
            throw new AuraHandledException(err);  
        }        
    }
   
    public static List<SObject> updateWOLIStatus(List<SObject> items, String actionVal){
        system.debug('##########closedAllWOLIs: '+items);
        List<WorkOrderLineItem> wolis = new List<WorkOrderLineItem>();
        for(SObject o: items){
            WorkOrderLineItem woli = (WorkOrderLineItem)o;

            if(actionVal == 'closedAll'){
                woli.Status = 'Closed';
            }else if(actionVal == 'approveAll'){
                woli.Substatus__c = 'Approved';
            }
            
            wolis.add(woli);
        }
        update wolis;
        
        return wolis;
    }

    public static List<SObject> updatePRLIStatus(List<SObject> items, String actionVal){
        system.debug('##########updatePRLIStatus: '+items+', actionVal: '+actionVal);
        if(items !=  null && items.size() >0){
            List<Parts_Request_Line_Item__c> prlis = new List<Parts_Request_Line_Item__c>();
            
            for(SObject o: items){
                Parts_Request_Line_Item__c prli = (Parts_Request_Line_Item__c)o;
                String newStatus = 'Submitted';
                if(actionVal == 'receiveAll'){
                    newStatus = 'Received';
                    prli.Received_Status__c = 'Received Satisfied';
                }else if(actionVal == 'shipAll'){
                    newStatus = 'Shipped';                
                }
    
                // ECP2-708 When WOLI is changed to submit or ship, Received_Status__c should not be Received Satisfied
                if(actionVal == 'shipAll' || actionVal == 'submitAll'){
                    prli.Received_Status__c = '';
                }
                prli.Status__c = newStatus;
                prlis.add(prli);
            }
            update prlis;
            Parts_Request_Line_Item__c tempPR = [SELECT Parts_Request__c from Parts_Request_Line_Item__c where id =: prlis[0].ID limit 1];
            Parts_Request__c pr = [SELECT Status__c from Parts_Request__c where id =: tempPR.Parts_Request__c];
            system.debug('########pr: '+pr);
            if(actionVal == 'receiveAll'){
                pr.Status__c = 'Received';
            }else if(actionVal == 'shipAll'){
                pr.Status__c = 'Shipped';            
            }else if (actionVal == 'submitAll'){
                pr.Status__c = 'Submitted'; 
            }
            update pr;
            
            return prlis; 
        }
        return null;       
    }

    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();

        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();

        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();

        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
        fieldMap.get(fld).getDescribe().getPickListValues();

        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }

    @AuraEnabled
    public static Map<String, List<String>> getAllSelectOptions(String objName, List<String> lstFld) {
        try {
            system.debug('objName --->' + objName);
            system.debug('lstFld --->' + lstFld);
            Map<String, List<String>> allOptsFlds = new Map<String, List<String>>();
            // Get the object type of the SObject.
            Schema.SObjectType objType = Schema.getGlobalDescribe().get(objName);

            // Describe the SObject using its object type.
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe();

            // Get a map of fields for the SObject
            Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();

            for (String fld : lstFld) {
                List<String> lstOps = new List<String>();

                // Get the list of picklist values for this field.
                List<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();

                // Add these values to the selectoption list.
                for (Schema.PicklistEntry a: values) {
                    lstOps.add(a.getValue());
                }
                system.debug('fld ---->' + fld);
                system.debug('lstOps ---->' + lstOps);
                lstOps.sort();

                allOptsFlds.put(fld, lstOps);
            }
            system.debug('allOptsFlds ---->' + allOptsFlds);
            return allOptsFlds;
        } catch (Exception ex) {
            System.debug('Error: ' + ex);
            throw new AuraHandledException(ex.getMessage());
        }
    }

    @AuraEnabled 
    public static user fetchUser(){     
        User oUser = [SELECT Id, Profile.Name, UserRole.Name 
                 FROM User Where id =: userInfo.getUserId()];
        system.debug('#########fetchUser: '+oUser);
        return oUser;
    }

    @AuraEnabled
    public static List <Commodity__c> fetchCommodity(String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        List <Commodity__c> returnList = new List <Commodity__c>();
        List <Commodity__c> lstOfCommodity = [select id, Name from Commodity__c where Name LIKE: searchKey];
     
        for (Commodity__c c: lstOfCommodity) {
            returnList.add(c);
        }
        return returnList;
     }
}