@isTest
private class AddPrimaryContactTest {
	private static List<Account> lstAccountCA;

	static {
		lstAccountCA = new List<Account>();
		List<Account> lstAccountNY = new List<Account>();
		for (Integer i = 0; i < 50; i++) {
			Account accCA = new Account();
			accCA.Name = 'Name';
			accCA.BillingState = 'CA';
			lstAccountCA.add(accCA);

			Account accNY = new Account();
			accNY.Name = 'Name';
			accNY.BillingState = 'NY';
			lstAccountNY.add(accNY);
		}

		insert lstAccountCA;
		insert lstAccountNY;
	}

    static testMethod void testQueue() {
        Contact cont = new Contact();
        cont.FirstName = 'FName';
        cont.LastName = 'LName';

        Test.startTest();
        	AddPrimaryContact objQueue = new AddPrimaryContact(cont, 'CA');
        	System.enqueueJob(objQueue);
		Test.stopTest();

		for (Account acc : lstAccountCA) {
			List<Contact> lstCont = [SELECT Id FROM Contact WHERE AccountId = :acc.Id];
			System.assertEquals(1, lstCont.size());
		}
    }
}