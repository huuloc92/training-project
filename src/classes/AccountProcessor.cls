public class AccountProcessor {

    @future
    public static void countContacts(List<Id> listAccId) {
    	List<Contact> lstCont = [SELECT Id, AccountId FROM Contact
    							WHERE AccountId IN :listAccId];
    	Map<Id, Integer> mapAcc = new Map<Id, Integer>();
    	for (Contact con : lstCont) {
    		if (mapAcc.containsKey(con.AccountId)) {
    			Integer noCon = mapAcc.get(con.AccountId);
    			noCon ++;
    			mapAcc.put(con.AccountId, noCon);
    		} else {
    			mapAcc.put(con.AccountId, 1);
    		}
    	}

    	List<Account> lstAcc = new List<Account>();
    	for (String accId : listAccId) {
    		Integer noCon = mapAcc.get(accId);
    		if (noCon == null) {
    			noCon = 0;
    		}
    		Account acc = new Account();
    		acc.Id = accId;
    		acc.Number_of_Contacts__c = noCon;
    		lstAcc.add(acc);
    	}

    	update lstAcc;
    }
}