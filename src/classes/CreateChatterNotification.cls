/*************************************************
* Class Name: CreateChatterNotification
* Author: Thom Phan
* Date: 24-May-2018
* Requirement/Project Name: Vantage
* Requirement/Project Create Chatter notification
*************************************************/
public class CreateChatterNotification{
   
    public CreateChatterNotification(String toUserId, String woId, String woName, String message){
    	
		ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
		input.subjectId = toUserId;
		input.feedElementType = ConnectApi.FeedElementType.FeedItem;

		ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegment;
		ConnectApi.MarkupBeginSegmentInput markupBeginSegment;
		ConnectApi.MarkupEndSegmentInput markupEndSegment;
		ConnectApi.EntityLinkSegmentInput entityLinkSegment;

		messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

		markupBeginSegment = new ConnectApi.MarkupBeginSegmentInput();
		markupBeginSegment.markupType = ConnectApi.MarkupType.Paragraph;
		messageInput.messageSegments.add(markupBeginSegment);

		textSegment = new ConnectApi.TextSegmentInput();
		textSegment.text = message;
		messageInput.messageSegments.add(textSegment);

		entityLinkSegment = new ConnectApi.EntityLinkSegmentInput();
		entityLinkSegment.entityId = woId;
		messageInput.messageSegments.add(entityLinkSegment);

		markupEndSegment = new ConnectApi.MarkupEndSegmentInput();
		markupEndSegment.markupType = ConnectApi.MarkupType.Paragraph;
		messageInput.messageSegments.add(markupEndSegment);

		input.body = messageInput;

		if(!Test.isRunningTest()){
			ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), input);
		}
    }  
}