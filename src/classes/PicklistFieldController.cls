/*************************************************
* Class Name: PicklistFieldController
* Author: Binh Dang 
* Date: 17-April-2018
* Requirement/Project Name: Vantage
* Requirement/Project Description: ECP2-645: Part Catalog UI
    - Controller for Picklist and Dependence Picklist on Part Catalog Lightning component.
    - Modify: Thom Phan 04/06/2018
*************************************************/
public class PicklistFieldController{    
    
    @AuraEnabled  
    public static Map<String,List<Map<String, String>>> getDependentOptionsDTO(string objApiName , string contrfieldApiName , string depfieldApiName){
        system.debug('getDependentOptionsDTO objApiName : '+objApiName + '##' + contrfieldApiName + '###' + depfieldApiName);
        
        String objectName = objApiName.toLowerCase();
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<Map<String, String>>> objResults = new Map<String,List<Map<String, String>>>();
        //get the string to sobject global map
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        
        if (!Schema.getGlobalDescribe().containsKey(objectName)){
            System.debug('OBJNAME NOT FOUND --.> ' + objectName);
            return null;
        }
        
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType==null){
            return objResults;
        }
        Bitset bitSetObj = new Bitset();
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        //Check if picklist values exist
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            System.debug('FIELD NOT FOUND --.> ' + controllingField + ' OR ' + dependentField);
            return objResults;     
        }
        
        List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();
        objFieldMap = null;
        System.debug('############contrEntries: '+contrEntries);
        System.debug('############depEntries: '+depEntries);
        List<Integer> controllingIndexes = new List<Integer>();
        for(Integer contrIndex=0; contrIndex<contrEntries.size(); contrIndex++){            
            Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
            String label = ctrlentry.getValue();
            //System.debug('#######label: '+label);
            objResults.put(label, new List<Map<String, String>>());
            controllingIndexes.add(contrIndex);
        }
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
        List<Bitset.PicklistEntryWrapper> objJsonEntries = new List<Bitset.PicklistEntryWrapper>();
        for(Integer dependentIndex=0; dependentIndex<depEntries.size(); dependentIndex++){            
            Schema.PicklistEntry depentry = depEntries[dependentIndex];
            objEntries.add(depentry);
        } 

        System.debug('#########controllingIndexes: '+controllingIndexes);
        System.debug('#########objEntries: '+objEntries);
        objJsonEntries = (List<Bitset.PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objEntries), List<Bitset.PicklistEntryWrapper>.class);
        List<Integer> indexes;

        System.debug('########objJsonEntries: '+objJsonEntries+', bitSetObj: '+bitSetObj);

        for (Bitset.PicklistEntryWrapper objJson : objJsonEntries){
            //System.debug('########objJson: '+objJson);
            if (objJson.validFor==null || objJson.validFor==''){
                continue;
            }
            //indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);
            String validForBits = Bitset.base64ToBits(objJson.validFor);
            //for (Integer idx : indexes){                
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                   String contrLabel = contrEntries[i].getValue();
                    //System.debug('#######contrLabel: '+contrLabel);
                    Map<String, String> picklistValue = new Map<String, String>();
                    picklistValue.put(objJson.value, objJson.label);
                    objResults.get(contrLabel).add(picklistValue);
                }

                
            }
        }
        objEntries = null;
        objJsonEntries = null;
        system.debug('objResults--->' + objResults);
        return objResults;
    }        
}