@isTest
private class WORunReportChatterNotificationBatchTest {
	
	static {
		createCS('Skip Triggers', true);
		User userCCM = createUser('CCM', 'CCM', 'Standard User');
		Home_Building_Facility__c objHBF = createHBF('HBFName', userCCM);
		Asset asset = createAsset('name', objHBF);
		User userAdmin = createUser('fName', 'lName', 'System Administrator');
		User userHBG = createUser('fName', 'lName', 'Home Building Facility');
		System.runAs(userAdmin) {
			WorkOrder wo = createWO(asset.Id);
			Test.setCreatedDate(wo.Id, System.now().addDays(-30));
			wo.OwnerId = userHBG.Id;
			update wo;

			List<Schema.PicklistEntry> values = WorkOrderLineItem.Compliance_Type__c.getDescribe().getPickListValues();
			String strCompliance = null;

			for (Schema.PicklistEntry a: values) {
				if (a.getValue() != 'No Further Action') {
					strCompliance = a.getValue();
					break;
				}
			}

			createWOLI(wo.Id, System.today().addDays(-15), strCompliance, false);
			createWOLI(wo.Id, System.today(), strCompliance, false);
		}
	}

	static testMethod void testExecuteBatch() {
		Test.startTest();
			WORunReportChatterNotificationBatch objBatch = new WORunReportChatterNotificationBatch();
			Database.executeBatch(objBatch);
		Test.stopTest();
	}
	
	private static User createUser(String fName, String lName, String profileName) {
		Profile profile = [SELECT Id FROM Profile WHERE Name = :profileName LIMIT 1];
		String dateString = 
				String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
		Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
		User u = new User(
						FirstName = fName,
						LastName = lName,
						Email = 'test' + dateString + randomInt + '@claytonhomes.com',
						Username = 'test' + dateString + randomInt + '@claytonhomes.com',
						ProfileId = profile.Id,
						Alias = 'jliv',
						TimeZoneSidKey = 'GMT',
						LanguageLocaleKey = 'en_US',
						EmailEncodingKey = 'UTF-8',
						LocaleSidKey = 'en_US');
		insert u;
		return u;
	}

	private static Asset createAsset(String name, Home_Building_Facility__c objHBF) {
		Asset asset = new Asset();
		asset.Name = name;
		asset.Home_Building_Facility_Name__c = objHBF.Id;
		insert asset;
		return asset;
	}

	private static Home_Building_Facility__c createHBF(String name, User user) {
		Home_Building_Facility__c objHBF = new Home_Building_Facility__c();
		objHBF.Name = name;
		objHBF.Customer_Care_Manager__c = user.Id;
		insert objHBF;
		return objHBF;
	}

	private static WorkOrder createWO(String assetId) {
		WorkOrder wo = new WorkOrder();
		wo.AssetId = assetId;
		insert wo;
		return wo;
	}

	private static WorkOrderLineItem createWOLI(String woId, Date complianceDate, 
												String compliance, Boolean determinationReport) {
		WorkOrderLineItem woli = new WorkOrderLineItem();
		woli.WorkOrderId = woId;
		woli.Compliance_Type_Date__c = complianceDate;
		woli.Compliance_Type__c = compliance;
		woli.Determination_Report__c = determinationReport;
		insert woli;
		return woli;
	}

	private static Common_Settings__c createCS(String name, Boolean isSkip) {
		Common_Settings__c cs = new Common_Settings__c();
		cs.Name = name;
		cs.Skip_Triggers__c = isSkip;
		insert cs;
		return cs;
	}
	
}