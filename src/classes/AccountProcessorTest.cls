@isTest
private class AccountProcessorTest {

	private static List<Account> lstAcc;
    static {
    	lstAcc = new List<Account>();
    	for (Integer i = 0; i < 5; i++) {
    		Account acc = new Account();
    		acc.Name = 'Name';
    		lstAcc.add(acc);
    	}
    	insert lstAcc;

    	List<Contact> lstCon = new List<Contact>();
    	for (Account acc : lstAcc) {
    		Contact con1 = new Contact();
    		con1.FirstName = 'Fname';
    		con1.LastName = 'Lname';
    		con1.AccountId = acc.Id;
    		lstCon.add(con1);

    		Contact con2 = new Contact();
    		con2.FirstName = 'Fname';
    		con2.LastName = 'Lname';
    		con2.AccountId = acc.Id;
    		lstCon.add(con2);
    	}
    	insert lstCon;

    	Account acc = new Account();
    	acc.Name = 'Name';
    	insert acc;
    	lstAcc.add(acc);
    }

    static testMethod void testCountContacts() {
    	List<String> lstAccId = new List<String>();
        for (Account acc : lstAcc) {
        	lstAccId.add(acc.Id);
        }
        Test.startTest();
        	AccountProcessor.countContacts(lstAccId);
        Test.stopTest();
    }
}