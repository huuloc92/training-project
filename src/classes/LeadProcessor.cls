global class LeadProcessor implements Database.Batchable<sObject> {
    global LeadProcessor() {
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, LeadSource FROM Lead';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Lead> lstLead) {
        for (Lead objLead : lstLead) {
            objLead.LeadSource = 'Dreamforce';
        }
        update lstLead;
    }

    global void finish(Database.BatchableContext BC) {

    }
}