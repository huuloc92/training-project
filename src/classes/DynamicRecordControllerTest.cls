/*************************************************************************************************
 * Class Name                       : DynamicRecordControllerTest
 * Author                           : Thom Phan
 * Date                             : 05/09/2018
 * Requirement/Project Name         : Test class for DynamicRecordController
 * Revision: Thom Phan 05/09/2018
 *************************************************************************************************/ 
@isTest 
private class DynamicRecordControllerTest{
    @testsetup
    static void setup() {
        createCustomSetting();
        WorkOrder wo1 = new WorkOrder(
                    Subject = 'Test 1',
                    Status = 'New',
                    Ready_for_Billback__c = false
                    );
        insert wo1;      
        
    }

    
    /**
    * @author Thom Phan
    * @date 05/09/2018
    * @description 
    * + getItemsList(String objApiName, String parentId, List<String> fieldNames, String parentFieldName)
    * + getselectOptions(sObject objObject, string fld)
    */
    static testMethod void testMethod_getItemsList() {      
        Test.startTest();
            WorkOrder wo = [Select id, Status, Ready_for_Billback__c FROM WorkOrder limit 1];
            List<String> fieldNames = new List<String> {'LineItemNumber','Status','Description'};
            DynamicRecordController.getItemsList('WorkOrderLineItem', 
            	String.valueOf(wo.ID), fieldNames, 'WorkOrderId');
            DynamicRecordController.getselectOptions(new WorkOrderLineItem(), 'Status');
            DynamicRecordController.fetchUser();
        Test.stopTest();
    }    

	/**
    * @author Thom Phan
    * @date 05/09/2018
    * @description 
    * + saveItems(List<SObject> items, String objApiName, String parentId, List<String> fieldNames, String parentFieldName)
    * + List<SObject> eAction(List<SObject> items, String objApiName, String actionVal)
    */
	static testMethod void testMethod_saveItems() {
		Test.startTest();
			WorkOrder wo = [Select id, Status, Ready_for_Billback__c FROM WorkOrder limit 1];

			List<SObject> sSObjects = new List<SObject>();
	        for(Integer i=0; i<3; i++){
	            WorkOrderLineItem lb = new WorkOrderLineItem();
	            lb.Status = 'Open';	            
	            lb.Workorderid = wo.Id;	  
	            SObject o = (SObject) lb;                         
	            sSObjects.add(o);
	        }
	        
	        insert sSObjects;

	        WorkOrderLineItem lb1 = new WorkOrderLineItem();
            lb1.Status = 'Open';	            
            lb1.Workorderid = wo.Id;	 
            SObject o = (SObject) lb1;                              
            sSObjects.add(o);

			List<String> fieldNames = new List<String> {'LineItemNumber','Status','Description'};
            DynamicRecordController.saveItems(sSObjects, 'WorkOrderLineItem', 
            	String.valueOf(wo.ID), fieldNames, 'WorkOrderId');
            DynamicRecordController.eAction(sSObjects, 'WorkOrderLineItem', 'closedAll');
		
        Test.stopTest();
	}

	/**
    * @author Thom Phan
    * @date 05/09/2018
    * @description 
    * + eAction(List<SObject> items, String objApiName, String actionVal)
    */
	static testMethod void testMethod_eAction() {
        ID recordTypeClayton = Schema.SObjectType.Parts_Request__c.getRecordTypeInfosByName().get('Clayton Parts Request').getRecordTypeId();
		Test.startTest();
            
			 Parts_Request__c pr = new Parts_Request__c(
                   Status__c = 'New', RecordTypeId = recordTypeClayton);
	        insert pr;

	        List<SObject> sSObjects = new List<SObject>();

	        for(Integer i= 0; i< 3; i++){
	            Parts_Request_Line_Item__c prli = new Parts_Request_Line_Item__c(Status__c ='New', Parts_Request__c = pr.ID);
	            SObject o = (SObject) prli;                         
	            sSObjects.add(o);
	        }

	        insert sSObjects;
			
            DynamicRecordController.eAction(sSObjects, 'Parts_Request_Line_Item__c', 'Submitted');  
            DynamicRecordController.eAction(sSObjects, 'Parts_Request_Line_Item__c', 'receiveAll');            
			DynamicRecordController.eAction(sSObjects, 'Parts_Request_Line_Item__c', 'shipAll');
        Test.stopTest();
	}

	/**
    * @author Thom Phan
    * @date 05/09/2018
    * @description 
    * + deleteItem(String valueId, String objApiName)
    */
	static testMethod void testMethod_deleteItem() {
		Test.startTest();
			WorkOrder wo = [Select id, Status, Ready_for_Billback__c FROM WorkOrder limit 1];

			List<SObject> sSObjects = new List<SObject>();
	        for(Integer i=0; i<3; i++){
	            WorkOrderLineItem lb = new WorkOrderLineItem();
	            lb.Status = 'Open';	            
	            lb.Workorderid = wo.Id;	  
	            SObject o = (SObject) lb;                         
	            sSObjects.add(o);
	        }
	        
	        insert sSObjects;	       

			
            DynamicRecordController.deleteItem(sSObjects[0].ID, 'WorkOrderLineItem');
            	
		
        Test.stopTest();
	}

    static void createCustomSetting() {
        Common_Settings__c cs = new Common_Settings__c();
        cs.Name = 'Skip Triggers'; 
        cs.Skip_Triggers__c = false; 
        insert cs;     
    }
}