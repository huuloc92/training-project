/*************************************************
* Class Name: BBComplianceChatterNotificationBatch
* Author: Thom Phan
* Date: 13-Jun-2018
* Requirement/Project Name: Vantage
* Requirement/Project 
* The "Compliance Type" field must be populated within 30 days of the WO being created. 
* If the BB was created 25 days ago and at least one of the WOLI Compliance Type is none, send Chatter notification to the CCM.
* Notification: "You have 5 days to enter the Compliance Type for the WOLIs on BB #". Need to show a link to the BB.
*************************************************/
public class BBComplianceChatterNotificationBatch implements Database.Batchable<sObject>, Database.Stateful {
	public BBComplianceChatterNotificationBatch() {
	}

	public List<Bill_Back__c> start (database.BatchableContext BC){
		Set<String> hbgUserProfile  = new Set<String>{'Home Building Facility', 'HBF Service Technician'};
		
		return [SELECT Id, OwnerId, 
						Name, 
						Work_Order__r.Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c 
				FROM Bill_Back__c 
				WHERE CreatedDate < LAST_N_DAYS:25 
						AND Work_Order__r.Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c != NULL 
						AND Owner.Profile.Name IN :hbgUserProfile
               			AND Status__c ='Submitted'];
	}
	
	public void execute(Database.BatchableContext BC, List<Bill_Back__c> bbList){
		System.debug('############bbList: '+bbList);
		List<Id> bbIDs = new List<ID>();
		if(bbList.size() > 0){
			for(Bill_Back__c bb: bbList){
				bbIDs.add(bb.ID);
			}

			Map<ID, List<WorkOrderLineItem>> workOrderLineItemMap = new Map<ID, List<WorkOrderLineItem>>();
			for(List<WorkOrderLineItem> wolis : [Select Id, 
														Bill_Back__c, Compliance_Type__c 
												FROM WorkOrderLineItem 
												WHERE Bill_Back__c  IN :bbIDs]){
				for(WorkOrderLineItem woli: wolis){
					if(workOrderLineItemMap.get(woli.Bill_Back__c) == null){
						List<WorkOrderLineItem> temp = new List<WorkOrderLineItem>();
						temp.add(woli);
						workOrderLineItemMap.put(woli.Bill_Back__c, temp);
					}else{
						workOrderLineItemMap.get(woli.Bill_Back__c).add(woli);
					}
				}
			}

			List<Bill_Back__c> bbNeedToSendChatterNotification = new List<Bill_Back__c>();

			for(Bill_Back__c bb: bbList){
				if(workOrderLineItemMap.get(bb.ID) != null){
					Boolean hasWOLINoneComplianceType = false;
					for(WorkOrderLineItem woli: workOrderLineItemMap.get(bb.ID)){
						if(woli.Compliance_Type__c == null){
							hasWOLINoneComplianceType = true;
						}
					}

					if(hasWOLINoneComplianceType){
						bbNeedToSendChatterNotification.add(bb);
					}
				}
			}
			
			System.debug('############bbNeedToSendChatterNotification: '+ bbNeedToSendChatterNotification);

			if(bbNeedToSendChatterNotification.size() >0){
				for(Bill_Back__c bb: bbNeedToSendChatterNotification){
					String message = 'You have 5 days to enter the Compliance Type for the WOLIs on ';
					new CreateChatterNotification(bb.Work_Order__r.Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c, bb.ID, bb.Name, message);
				}
			}
		}
	}

	public void finish(Database.BatchableContext BC){
		AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed,TotalJobItems,CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
		Messaging.SingleEmailMessage batchEmail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] {a.CreatedBy.Email};
		batchEmail.setToAddresses(toAddresses);
		batchEmail.setSubject('Chatter notification status: ' + a.Status);
		batchEmail.setPlainTextBody('Chatter notification with '+ a.NumberOfErrors + ' failures.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {batchEmail});
	}
}