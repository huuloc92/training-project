/*************************************************
* Class Name: WORunReportChatterNotificationBatch 
* Author: Thom Phan
* Date: 24-May-2018
* Requirement/Project Name: Vantage
* Requirement/Project 
* 2.    The comments/acceptance criteria on JIRA for the 2nd batch are also pretty clear now 
* a.  If the compliance type date was 15 days ago AND 
* b.  if the compliance type is anything other than ‘No Further Action’ AND
* c.  the checkbox ‘Did you run the Determination Report?’ is not checked THEN
* d.  a chatter notification should be triggered to the HBF CCM.
* i.  Chatter message : ‘You have 5 days to run the Determination Report and check the box for the WOLIs on WO-00000194’
*************************************************/
public class WORunReportChatterNotificationBatch implements Database.Batchable<sObject>, Database.Stateful {

    //public string query;
    
    public WORunReportChatterNotificationBatch (){
        // consider CreatedDate < LAST_N_DAYS:25 to get smaller group, should be checked with Roshan again
        //this.query = 'SELECT Id, ownerID, WorkOrderNumber, Work_Order_HyperLink__c, Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c FROM WorkOrder WHERE CreatedDate < LAST_N_DAYS:25 AND Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c != NULL';
    }
    
    //public Database.QueryLocator start (database.BatchableContext BC){
    //    return Database.getQueryLocator(query);
    //}

    public List<WorkOrder> start (database.BatchableContext BC){
        Set<String> hbgUserProfile  = new Set<String>{'Home Building Facility', 'HBF Service Technician'};
        
        return [SELECT Id, OwnerId, 
                        WorkOrderNumber, 
                        Work_Order_HyperLink__c,
                        Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c 
                FROM WorkOrder 
                WHERE CreatedDate < LAST_N_DAYS:25 
                        AND Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c != NULL 
                        AND Owner.Profile.Name IN :hbgUserProfile];
    }
    
    public void execute(Database.BatchableContext BC, List<WorkOrder> woList){
        System.debug('############woList: '+woList);
        List<Id> woIDs = new List<ID>();
        if(woList.size() > 0){            
            for(WorkOrder wo: woList){
                woIDs.add(wo.ID);
            }

            Map<ID, List<WorkOrderLineItem>> workOrderLineItemMap = new Map<ID, List<WorkOrderLineItem>>();
            for(List<WorkOrderLineItem> wolis : [Select Id, WorkOrderID, Compliance_Type__c,Determination_Report__c, Compliance_Type_Date__c 
                                                                FROM WorkOrderLineItem 
                                                                WHERE WorkOrderID  in: woIDs]){
                for(WorkOrderLineItem woli: wolis){
                    if(workOrderLineItemMap.get(woli.WorkOrderID) == null){
                        List<WorkOrderLineItem> temp = new List<WorkOrderLineItem>();
                        temp.add(woli);
                        workOrderLineItemMap.put(woli.WorkOrderID, temp);
                    }else{
                        workOrderLineItemMap.get(woli.WorkOrderID).add(woli);
                    }
                }
            }            

            List<WorkOrder> woNeedToSendChatterNotification = new List<WorkOrder>();

            for(WorkOrder wo: woList){
                if(workOrderLineItemMap.get(wo.ID) != null){
                    Boolean hasWOLIInNotCondition = false;
                    for(WorkOrderLineItem woli: workOrderLineItemMap.get(wo.ID)){
                        
                        if(woli.Compliance_Type_Date__c != null && (woli.Compliance_Type_Date__c + 14 <= System.today()) && woli.Compliance_Type__c != 'No Further Action'
                            && !woli.Determination_Report__c){                            
                            hasWOLIInNotCondition = true;
                        }
                    }

                    if(hasWOLIInNotCondition){
                        woNeedToSendChatterNotification.add(wo);
                    }
                }
            }
            
            System.debug('############woNeedToSendChatterNotification: '+woNeedToSendChatterNotification);

            if(woNeedToSendChatterNotification.size() >0){
                for(WorkOrder wo: woNeedToSendChatterNotification){
                    String message = 'You have 5 days to run the Determination Report and check the box for the WOLIs on ';
                    //ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), wo.Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c, ConnectApi.FeedElementType.FeedItem, message);
                    //new CreateChatterNotification(wo.Asset.Home_Building_Facility_Name__r.Customer_Care_Manager__c, wo.ID, wo.WorkOrderNumber, message);
                    new CreateChatterNotification(wo.OwnerId, wo.ID, wo.WorkOrderNumber, message);
                }
            }            
        }        
    }

    public void finish(Database.BatchableContext BC){        
        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed,TotalJobItems,CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
        Messaging.SingleEmailMessage batchEmail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        batchEmail.setToAddresses(toAddresses);
        batchEmail.setSubject('Chatter notification status: ' + a.Status);
        batchEmail.setPlainTextBody('Chatter notification with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {batchEmail});
    }

}