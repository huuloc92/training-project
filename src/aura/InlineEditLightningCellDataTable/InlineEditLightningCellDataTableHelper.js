({
    fetchPickListVal: function(component, fieldName, elementId, cvalue) {
        // var action = component.get("c.getselectOptions");
        // action.setParams({
        //     "objObject": component.get("v.objVal"),
        //     "fld": fieldName
        // });
        // var opts = [];
        // action.setCallback(this, function(response) {
        //     if (response.getState() == "SUCCESS") {
        //         var allValues = response.getReturnValue();

        //         if (allValues != undefined && allValues.length > 0) {
        //             opts.push({
        //                 class: "optionClass",
        //                 label: "--- None ---",
        //                 value: ""
        //             });
        //         }
        //         for (var i = 0; i < allValues.length; i++) {
        //             if (cvalue === allValues[i]) {
        //                 opts.push({
        //                     class: "optionClass",
        //                     label: allValues[i],
        //                     value: allValues[i],
        //                     selected: "true"
        //                 });
        //             } else {
        //                 opts.push({
        //                     class: "optionClass",
        //                     label: allValues[i],
        //                     value: allValues[i]
        //                 });
        //             }
        //         }
        //         component.find(elementId).set("v.options", opts);
        //     }
        // });
        // $A.enqueueAction(action);
        var pickListValueList = JSON.parse(component.get("v.picklistValueMap"));
        var allValues = pickListValueList[fieldName];
        if (allValues != undefined && allValues.length > 0) {
            var opts = [];
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
            for (var i = 0; i < allValues.length; i++) {
                if (cvalue === allValues[i]) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i],
                        selected: "true"
                    });
                } else {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
            }
        }
        component.find(elementId).set("v.options", opts);
    },

    convertFixTo2Decimal : function (component, inputVal) { 
        if(inputVal.includes('.')){
            var fields = inputVal.split('.');        
            var main = fields[0].length > 2 ? fields[0].substring(0, 2) : fields[0];
            var sub = fields[1].length > 2 ? fields[1].substring(0, 2) : fields[1];            
            var result = main + '.' + sub ;
            component.set("v.fieldValue", Number(result));
        }else{
            if(Number(inputVal)<0) component.set("v.fieldValue", 1);
            if(Number(inputVal)>99) component.set("v.fieldValue", Number(inputVal.substring(0, 2)) );
        }
    },
})