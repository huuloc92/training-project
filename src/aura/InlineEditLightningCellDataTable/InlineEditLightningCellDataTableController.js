/*************************************************
 * Author: Thom Phan
 * Date: 23-Apr-2018
 * Description: handle js to display value for each cell  
 *************************************************/
({
    doInit: function(component, event, helper) {
        var objVal      = component.get('v.objVal');
        var fieldName   = component.get('v.fieldName');
        //console.log('###objVal: '+JSON.stringify(objVal)+',fieldName: '+fieldName);
        if (fieldName != undefined) {
            var dataType    = fieldName.split('+')[1];
            var fieldName   = fieldName.split('+')[0];
            var cellIndex   = component.get('v.cellIndex');
            var isReadOnly  = component.get('v.isReadOnly');
            //console.log('#########dataType: '+dataType+', fieldName: '+fieldName);
            //console.log('##########objVal[fieldName] : '+objVal[fieldName]);
            component.set('v.dataType', dataType);
            component.set('v.fieldName', fieldName);
            
            if (fieldName.indexOf(".") >= 0) {
                var parentSobject = objVal[fieldName.split(".")[0]];
                if (parentSobject != undefined) {
                    component.set('v.fieldValue', parentSobject[fieldName.split(".")[1]]);
                }
            } else {
                if (cellIndex == 0) {
                    var number_temp = objVal[fieldName].split('-')[1];
                    var number = '';
                    if( number_temp ) {
                        number = number_temp;
                    } else {
                        number = objVal[fieldName].split('-')[0];
                    }                    
                    component.set('v.fieldValue', number);
                } else {
                    if (dataType == 'inputSelect' && isReadOnly == false) {
                        helper.fetchPickListVal(component, fieldName, 'inputTextID', objVal[fieldName]);
                    }
                    component.set('v.fieldValue', objVal[fieldName]);
                }
                console.log('###cell fieldValue: '+component.get('v.fieldValue'));
            }
        }

    },
    
    itemsChange: function(component, event, helper) {
        var objVal          = component.get('v.objVal');
        var fieldName       = component.get('v.fieldName');
        objVal[fieldName]   = component.get('v.fieldValue');
        component.set('v.objVal', objVal);
    },
    
    dropdownChange: function(component, event, helper) {
        var objVal          = component.get('v.objVal');
        var fieldName       = component.get('v.fieldName');
        var oldFieldValue   = component.get('v.fieldValue');
        var selectedValue   = component.find("inputTextID").get("v.value");
        if (selectedValue !== oldFieldValue) {
            component.set('v.fieldValue', selectedValue);
        }
        objVal[fieldName] = component.get('v.fieldValue');
        component.set('v.objVal', objVal);
    },

    limitInputNumber : function(component, event, helper) {        
        if(component.get('v.fieldName') == 'Quantity__c' && component.get('v.dataType') == 'inputNumber'){
            var inputVal = String(event.getSource().get('v.value'));
            helper.convertFixTo2Decimal(component, inputVal);
        }
    },
})