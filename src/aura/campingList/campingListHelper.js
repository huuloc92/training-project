({
	getCampingItems : function(component) {
		var action = component.get("c.getItems");
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.items", response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	}
})