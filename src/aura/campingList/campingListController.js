({
	// function call on component Load
	doInit: function(component, event, helper) {
		// create a Default RowItem [Contact Instance] on first time Component Load
		// by call this helper function  
		// get the contactList from component and add(push) New Object to List  

		var action = component.get("c.getItems");
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.items", response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	},
	handleAddItem: function(component, event, helper) {
		var newItem = event.getParam("item");
		var action = component.get("c.saveItem");
		action.setParams({
			"objCampingItem": newItem
		});
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS") {
				var listItems = component.get("v.items");
				listItems.unshift(response.getReturnValue());
				component.set("v.items", listItems);
			}
		});
		$A.enqueueAction(action);
	}
})