({
	doInit : function(component, event, helper) {
		var objId = component.get("v.recordId");
		var objName = component.get("v.sObjectName");
		var strType = component.get("v.type");
		if (!objId || !strType ||
				(strType != "Case" && strType!= "WorkOrder"
					&& strType != "PartsRequest"
					&& strType != "BillBack")) {
			var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "type":     "error",
                "title":    "Error",
                "message":  "Check input on attributes again."
            });  
            resultsToast.fire();
		} else if (objName != 'Account' && objName != 'Asset') {
			var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "type":     "error",
                "title":    "Error",
                "message":  "This page is not Account or Asset page."
            });  
            resultsToast.fire();
		} else {
			helper.getStatistics(component, objId, objName, strType);
		}
	}
})