({
	getStatistics : function(component, objId, objName, strType) {
		var action = component.get("c.getStatistics");
		action.setParams({
			"objId" : objId,
			"objName" : objName,
			"strType" : strType
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				if (response.getReturnValue()) {
					component.set("v.total", response.getReturnValue());
				}
			}
		});
		$A.enqueueAction(action);
	}
})