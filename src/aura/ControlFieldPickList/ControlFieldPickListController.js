({
    doInit : function(component, event, helper) {
        var objectApiName = component.get("v.objectName");
        var fieldControl = component.get("v.fieldControl");
        var fieldDependent = component.get("v.fieldDependent");        
       // console.log('###########selectedValue on controlField'+component.get("v.selectedValue"));
        helper.fetchPicklistValues(component, event, objectApiName, fieldControl, fieldDependent );    
    },
    // function call on change tha controller field  
    onControllerFieldChange: function(component, event, helper) {
        var selectedValue = event.getSource().get("v.value");
        helper.getDependPickList(component, event, selectedValue);        
    },
})