({
	fetchPicklistValues : function(component, event, objectApiName, fieldControl, fieldDependent) {
        // call the server side function  
        var action = component.get("c.getDependentOptionsDTO");
        action.setParams({
            'objApiName': objectApiName,
            'contrfieldApiName': fieldControl,
            'depfieldApiName': fieldDependent
        });
        action.setCallback(this, function(response) {
            console.log('#######response.getState() :'+response.getState());
            if (response.getState() == "SUCCESS") {
                //store the return response from server (map<string,List<string>>)  
                var storeResponse = response.getReturnValue();
                var cmp = component.get("v.depnedentFieldMap");
                component.set("v.depnedentFieldMap", storeResponse);
                // create a empty array for store map keys(@@--->which is controller picklist values) 
                
                var listOfkeys = []; // for store all map keys (controller picklist values)
                var controllerField = []; // for store controller picklist value to set on ui field. 
                
                // play a for loop on Return map 
                // and fill the all map key on listOfkeys variable.
                for (var singlekey in storeResponse) {
                    listOfkeys.push(singlekey);
                }
                
                //set the controller field value for ui:inputSelect  
                if (listOfkeys != undefined && listOfkeys.length > 0) {
                    controllerField.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: "",
                        selected: false
                    });
                }                               

                for (var i = 0; i < listOfkeys.length; i++) {                   
                    if (component.get('v.selectedValue') == listOfkeys[i]) {                        
                        controllerField.push({
                            class: "optionClass",
                            label: listOfkeys[i],
                            value: listOfkeys[i],
                            selected: true
                        });
                    } else {
                         controllerField.push({
                            class: "optionClass",
                            label: listOfkeys[i],
                            value: listOfkeys[i],
                            selected: false
                        });
                    }
                   
                }
                            
                component.set("v.controllOptions", controllerField);                
                if(component.get('v.selectedValue') != null && component.get('v.selectedValue') != undefined
                    && component.get('v.selectedValue') != '--- None ---' && component.get('v.selectedValue') != ""){
                    this.getDependPickList(component, event, component.get('v.selectedValue'));
                }
                
            }
        });
        $A.enqueueAction(action);
        
    },
    getDependPickList: function(component, event, selectedValue) {
        
        if(selectedValue == '--- None ---' || selectedValue == undefined){
            selectedValue = "";
        }
        component.set("v.selectedValue", selectedValue);        
        var controllerValueKey = selectedValue;
        
        // get the map values   
        var Map = component.get("v.depnedentFieldMap");
        var dependentFields = [];
        // check if selected value is not equal to None then call the helper function.
        // if controller field value is none then make dependent field value is none and disable field
        if (controllerValueKey != '-- None --' && controllerValueKey != '--- None ---' && controllerValueKey != "") {
            
            // get dependent values for controller field by using map[key].  
            // for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
            // map['India'] = its return all dependent picklist values.
            
            var ListOfDependentFields = Map[controllerValueKey];
            if (ListOfDependentFields != undefined && ListOfDependentFields.length > 0) {
                dependentFields.push({
                    class: "optionClass",
                    label: "--- None ---",
                    value: "",
                    selected: false
                });
                
            }            


            for (var i = 0; i < ListOfDependentFields.length; i++) {
                var mapPicklist = ListOfDependentFields[i];
                for(var key in mapPicklist){
                    dependentFields.push({
                        class: "optionClass",
                        label: mapPicklist[key],
                        value: key,
                        selected: false
                    });
                }
            }
            console.log('########:dependentFields: '+dependentFields);
        } else {
            var dependentFields = [{
                class: "optionClass",
                label: '--- None ---',
                value: "",
                selected: false
            }];
        }
        
        var appEvent = $A.get("e.c:onChangeControlField");
        appEvent.setParams({           
            "dependValueList" : dependentFields,
            "controlField": component.get("v.fieldControl"),
            "objectName" : component.get("v.objectName"),
            "rowIndex" : component.get("v.rowIndex")
        });
        appEvent.fire();
        
    },
})