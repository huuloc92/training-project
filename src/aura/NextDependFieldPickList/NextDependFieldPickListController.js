({
	doInit : function(component, event, helper) {
		var defaultVal = [{
            class: "optionClass",
            label: '--- None ---',
            value: ''
        }];
        component.set("v.dependentOptions", defaultVal);
        //console.log('####dependfield: '+component.get("v.selectedDependValue"));
	},

    handleChangeNextDependList : function(component, event, helper){        
        var dependValueList = event.getParam("nextDependValueList");
        var controlField = event.getParam("controlField");
        var objectName = event.getParam("objectName");        
        var rowIndex = event.getParam("rowIndex");
        
        if(rowIndex == null || rowIndex == component.get('v.rowIndex')){
            
            component.set("v.isDependentDisable", true);            
            if((controlField == component.get("v.fieldControl") || controlField === component.get("v.fieldControl"))
              && (objectName == component.get("v.objectName") || controlField === component.get("v.objectName"))){
                var selectedDependValue = component.get("v.selectedDependValue");  
                //console.log('#####handleChangeDependList: '+selectedDependValue);              
                for (var i = 0; i < dependValueList.length; i++) {
                    if(dependValueList[i].value != "" 
                        && selectedDependValue != ""
                        && dependValueList[i].value == selectedDependValue){
                        dependValueList[i].selected = true;
                    }
                }
                console.log('#########')
                component.set("v.dependentOptions", dependValueList);
                if(component.get("v.dependentOptions").length > 1){
                    component.set("v.isDependentDisable", false);
                }else{
                    component.set("v.selectedDependValue", "");
                }
            }else{
                var dependValueList = [{
                    class: "optionClass",
                    label: '--- None ---',
                    value: ''
                }];
                component.set("v.dependentOptions", dependValueList);
            }
        }
    },    
})