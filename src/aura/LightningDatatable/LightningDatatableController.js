({
    init: function (component, event, helper) {
        var columns = [
        	{label: 'Account Id', fieldName: 'Id', type: 'url', typeAttributes: { label: { fieldName : 'Name'}, target: '_blank'}},
            {label: 'Account name', fieldName: 'Name', type: 'text', sortable: true},
            {label: 'Phone', fieldName: 'Phone', type: 'phone', sortable: false},
            {label: 'Email', fieldName: 'Email', type: 'email', sortable: false},
        ];
        component.set("v.mycolumns",columns);
        var action = component.get("c.getContacts");
        action.setCallback(this,function(res){
            var state = res.getState();
            if(state=== 'SUCCESS'){
                console.log(res.getReturnValue());
                component.set("v.mydata",res.getReturnValue());
            }
        });
        $A.enqueueAction(action);

    },
})