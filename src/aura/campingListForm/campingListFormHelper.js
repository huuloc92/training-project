({
	createItem : function(component) {
		var validItem = component.find('campingItemForm').reduce(function(validSoFar, inputCmp) {
			inputCmp.showHelpMessageIfInvalid();
			return validSoFar && inputCmp.get('v.validity').valid;
		}, true);
		if (validItem) {
			var createEvent = $A.get("e.c:addItemEvent");
			var newItem = component.get("v.newItem");
			createEvent.setParams({ "item": newItem });
        	createEvent.fire();

        	component.set("v.newItem", {'SObjectType':'Camping_Item__c',
							'Name':'',
							'Price__c':0,
							'Quantity__c':0,
							'Packed__c': false,
							'Account__c': null});
		}
	}
})