({
	render : function(component, helper) {
		var ret = this.superRender();
		console.log("Phase render");
		return ret;
	},
	rerender : function(component, helper) {
		this.superRerender();
		console.log("Phase rerender");
	},
	afterRender : function(component, helper) {
		this.superAfterRender();
		console.log("Phase afterRender");
	},
	unrender : function(component, helper) {
		this.superUnrender();
		console.log("Phase unrender");
	}
})