({
	doInit : function(component, event, helper) {
		console.log("Phase doInit");
	},
	doValueRender : function(component, event, helper) {
		console.log("Phase doValueRender");
	},
	doDoneRender : function(component, event, helper) {
		console.log("Phase doDoneRender");
	},
	doRefreshView : function(component, event, helper) {
		console.log("Phase doRefreshView");
	},
	doEvent : function(component, event, helper) {
		component.set("v.attTemp", "def");
		console.log("Phase doEvent");
	},
	fireEvent : function(component, event, helper) {
		console.log("Phase START fireEvent");
		var cmpEvent = component.getEvent("evtComp");
        cmpEvent.fire();
        console.log("Phase END fireEvent");
	},
	closeChild : function(component, event, helper) {
		console.log("Phase START closeChild");
		var isChild = component.get("v.isChild");
		if (isChild) {
			component.set("v.isChild", false);
		} else {
			component.set("v.isChild", true);
		}
        console.log("Phase END closeChild");
	},
	changeValue : function(component, event, helper) {
		console.log("Phase changeValue");
	},
	sendRequest : function(component, event, helper) {
		console.log("Phase START sendRequest");
		var action = component.get("c.tempAction");
		action.setCallback(this, function(response) {
		});
		$A.enqueueAction(action);
		console.log("Phase END sendRequest");
	},
	doWaiting : function(component, event, helper) {
		console.log("Phase doWaiting");
	},
	doDoneWaiting : function(component, event, helper) {
		console.log("Phase doDoneWaiting");
	},
	sendErrorRequest : function(component, event, helper) {
		console.log("Phase sendErrorRequest");
		var error = new $A.auraFriendlyError("This is a sample error.");
		// set an optional error data object
		error.data = {
			"moreErrorData1": "more1",
			"moreErrorData2": "more2",
		};
		throw error;
	},
	doError : function (component, event, helper) {
		console.log("Phase doError");
	}
})