({
    fetchNextDependentPicklistValues : function(component, event){   
        console.log('#####fetchNextDependentPicklistValues');
        var objectApiName = component.get("v.objectName");
        var fieldControl = component.get("v.nextFieldControl");
        var fieldDependent = component.get("v.nextFieldDependent");

        var action = component.get("c.getDependentOptionsDTO");
        action.setParams({
            'objApiName': objectApiName,
            'contrfieldApiName': fieldControl,
            'depfieldApiName': fieldDependent
        });

        action.setCallback(this, function(response) {
            console.log('#######response.getState() :'+response.getState());
            if (response.getState() == "SUCCESS") {
                //store the return response from server (map<string,List<string>>)  
                var storeResponse = response.getReturnValue();
                var cmp = component.get("v.nextdepnedentFieldMap");
                component.set("v.nextdepnedentFieldMap", storeResponse);    
                              
                /*if(component.get('v.selectedDependValue') != null && component.get('v.selectedDependValue') != undefined
                    && component.get('v.selectedDependValue') != '--- None ---' && component.get('v.selectedDependValue') != ""){
                    this.getNextDependPickList(component, event, component.get('v.selectedDependValue'));
                }*/
                this.getNextDependPickList(component, event, component.get('v.selectedDependValue'));
            }
        });
        $A.enqueueAction(action);
    },  

    getNextDependPickList: function(component, event, selectedDependValue) {       
        var controllerValueKey = selectedDependValue;
        
        // get the map values   
        var Map = component.get("v.nextdepnedentFieldMap");
        var dependentFields = [];
        // check if selected value is not equal to None then call the helper function.
        // if controller field value is none then make dependent field value is none and disable field
        if (controllerValueKey != '-- None --' && controllerValueKey != '--- None ---'
            && controllerValueKey != "") {
            
            // get dependent values for controller field by using map[key].  
            // for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
            // map['India'] = its return all dependent picklist values.
            
            var ListOfDependentFields = Map[controllerValueKey];
            if (ListOfDependentFields != undefined && ListOfDependentFields.length > 0) {
                dependentFields.push({
                    class: "optionClass",
                    label: "--- None ---",
                    value: "",
                    selected: false
                });
                
            }            


            for (var i = 0; i < ListOfDependentFields.length; i++) {
                var mapPicklist = ListOfDependentFields[i];
                for(var key in mapPicklist){
                    dependentFields.push({
                        class: "optionClass",
                        label: mapPicklist[key],
                        value: key,
                        selected: false
                    });
                }
            }
            console.log('########:dependentFields: '+dependentFields);
        } else {
            var dependentFields = [{
                class: "optionClass",
                label: '--- None ---',
                value: "",
                selected: false
            }];
        }
        
        var appEvent = $A.get("e.c:onChangeNextControlField");
        appEvent.setParams({           
            "nextDependValueList" : dependentFields,
            "controlField": component.get("v.nextFieldControl"),
            "objectName" : component.get("v.objectName"),
            "rowIndex" : component.get("v.rowIndex")
        });
        appEvent.fire();
        
    },
})