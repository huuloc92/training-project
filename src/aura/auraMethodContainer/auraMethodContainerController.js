({
	doIt : function(component, event, helper) {
		var childComp = component.find("child");
		if (childComp.length && childComp.length > 1) {
			for (var comp in childComp) {
				childComp[comp].doSomething();
			}
		} else {
			childComp.doSomething();
		}
	}
})