/*************************************************
 * Author: Thom Phan
 * Date: 23-Apr-2018
 * Description: handle js control dynamic row   
 *************************************************/
({
    doInit: function(component, event, helper) {
        helper.getAllItemsList(component, event);
        var extraAction = component.get('v.extraAction');
        var methods = [];
        var methodDescriptions = [];
        for (var key in extraAction) {
            methods.push(key);
            methodDescriptions.push(extraAction[key]);
        }
        component.set('v.extraActionMethod', methods);
        component.set('v.extraActionDescription', methodDescriptions);
        
        var headerTextList = component.get('v.headerText');
        if (headerTextList.length != 0) {
            if (typeof headerTextList[0] === 'string' || headerTextList[0] instanceof String) {
                var hdList = [];
                for (var i = 0; i < headerTextList.length; i++) {
                    var headerText = headerTextList[i];
                    var text = headerText.split('+')[0];
                    var required = headerText.split('+')[1];
                    if (required != undefined) required = required.trim() + ' ';
                    else required = '';
                    var hdTemp = {
                        "text": text,
                        "required": required
                    };
                    hdList.push(hdTemp);
                }
                component.set('v.headerText', hdList);
            }
        }
    },
    
    saveData: function(component, event, helper) {
        helper.saveData(component, event, 0, false);
    },
    
    addNewRow: function(component, event, helper) {
        helper.createObjectData(component, event);
    },
    
    removeDeletedRow: function(component, event, helper) {
        var index = event.getParam("indexVar");
        var allRowsList = component.get("v.listOfRecord");
        
        // remove on create form
        var supportAction = component.get("v.actionSupport");
        if (supportAction == 'createAndDelete') {
            allRowsList = component.get("v.listOfNewRecord");
        }
        
        allRowsList.splice(index, 1);
        
        if (supportAction == 'createAndDelete') {
            component.set("v.listOfNewRecord", allRowsList);
        } else {
            component.set("v.listOfRecord", allRowsList);
        }
        
        /* Requirement is changed, DONT delete existed item
        var valID       = allRowsList[index].Id;
        var objectName  = component.get("v.objectName");

        if(valID != ''){
            var action = component.get("c.deleteItem");
            action.setParams({
                "objApiName": objectName,
                "valueId": valID
            });
            
            action.setCallback(this, function(response) {      
                var resultsToast = $A.get("e.force:showToast");          
                if (response.getState() === "SUCCESS") {                    
                    $A.get('e.force:refreshView').fire(); 
                }else{
                    var errors = response.getError();
                    var message = 'Unknown error'; 
                    if (errors && Array.isArray(errors) && errors.length > 0) {
                        message = errors[0].message;
                    }
                    resultsToast.setParams({
                        "type":     "error",
                        "title":    "Review the errors on this page.",
                        "message":  message
                    });   
                }
            });
            $A.enqueueAction(action);
        }   */
        },
    
    extraAction: function(component, event, helper) {
        var selectIndex = event.currentTarget.value;
        helper.saveData(component, event, selectIndex, true);
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true);
    },
    
    hideSpinner: function(component, event, helper) {
        component.set("v.spinner", false);
    }
    
})