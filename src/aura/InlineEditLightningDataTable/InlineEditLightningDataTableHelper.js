({
    getItemsList : function(component, event) {
        
        var parentId        = component.get("v.parentId");
        var parentFieldName = component.get("v.parentFieldName");
        var objectName      = component.get("v.objectName");
        var fieldNames      = component.get("v.fieldNames");
        
        var action = component.get("c.getItemsList");
        action.setParams({
                "parentId": parentId, 
                "parentFieldName": parentFieldName,
                "fieldNames": fieldNames, 
                "objApiName": objectName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                component.set("v.listOfRecord",response.getReturnValue()); 
            }else{
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type":     "error",
                    "title":    "Error",
                    "message":  response.getErrors()
                });  
                resultsToast.fire();
            }            
        });
        $A.enqueueAction(action); 
    },

    createObjectData: function(component, event) {
          
        var rowItemList     = component.get("v.listOfRecord");
        var rowNewItemList  = component.get("v.listOfNewRecord");        
        var parentId        = component.get("v.parentId");
        var objectName      = component.get("v.objectName");
        var fieldNames      = component.get('v.fieldNames');
        var parentFieldName = component.get("v.parentFieldName");
        var defaultValues   = component.get("v.defaultFieldValues");
        var supportAction   = component.get("v.actionSupport");

        if (fieldNames != undefined) {  
            var objVal = {};
            objVal['sobjectType'] = objectName;
            objVal[parentFieldName] = parentId;

            for(var i = 0; i < fieldNames.length; i++){
                var fieldName = fieldNames[i].split('+')[0];
                if(defaultValues != null && defaultValues != undefined && defaultValues[fieldName] != undefined && defaultValues[fieldName] != '' && defaultValues[fieldName] != null){
                    objVal[fieldName] = defaultValues[fieldName];
                }else{
                    objVal[fieldName] = '';
                }
            }                              
                         
            if(supportAction=='createAndDelete'){
               rowNewItemList.push(objVal);    
               component.set("v.listOfNewRecord", rowNewItemList);     
            }else{
               rowItemList.push(objVal);  
               component.set("v.listOfRecord", rowItemList);
            }            
        }
           
    },
    
    saveData: function(component, event, selectIndex, isExtraAction) {        
        if (this.validateRequired(component, event)) {
            var parentId            = component.get("v.parentId");
            var parentFieldName     = component.get("v.parentFieldName");
            var objectName          = component.get("v.objectName");
            var fieldNames          = component.get("v.fieldNames");            
            var listNeedToBeUpdated = component.get("v.listOfRecord");
            
            // if support for create only, the list should be new list
            if(component.get("v.actionSupport") == 'createAndDelete'){
                listNeedToBeUpdated = component.get("v.listOfNewRecord");
            }        
            
            if(listNeedToBeUpdated != null || listNeedToBeUpdated != undefined){
                var action = component.get("c.saveItems");                
                action.setParams({
                    "parentId":         parentId, 
                    "parentFieldName":  parentFieldName,
                    "fieldNames":       fieldNames, 
                    "objApiName":       objectName,
                    "items":            listNeedToBeUpdated
                });    
                
                action.setCallback(this, function(response) {
                    var state = response.getState();                   
                    var resultsToast = $A.get("e.force:showToast");
                    
                    if (state === "SUCCESS") {
                        var res = response.getReturnValue();                        
                        if(res != null){       
                            component.set("v.listOfRecord", response.getReturnValue());  
                            component.set("v.listOfNewRecord", []);       
                            if(isExtraAction == true){
                                this.callExtraAction(component, event, selectIndex);
                            }else{           
                                resultsToast.setParams({
                                    "type":     "success",
                                    "title":    "Confirmation",
                                    "message":  component.get('v.messagesAction')['save']
                                });                          
                                $A.get('e.force:refreshView').fire(); 
                            }
                        }else{                            
                            resultsToast.setParams({
                                "type":     "error",
                                "title":    "Error",
                                "message":  "There is an error so these items can not be updated"
                            }); 
                        }
                    }else{
                        var errors = response.getError();
                        var message = 'Unknown error'; 
                        if (errors && Array.isArray(errors) && errors.length > 0) {
                            message = errors[0].message;
                        }
                        resultsToast.setParams({
                            "type":     "error",
                            "title":    "Review the errors on this page.",
                            "message":  message
                        });    
                    }
                    resultsToast.fire();
                });
                 
                $A.enqueueAction(action);
            }            
        }
    },     

    //there is a new line item and if the user hits “Extra Action” then you should perform and insert new line item and update all again
    callExtraAction: function(component, event, selectIndex) {
        console.log('callExtraAction isExtraAction: '+selectIndex);
        var objectName  = component.get("v.objectName");            
        var method      = component.get("v.extraActionMethod")[selectIndex];            
        var action      = component.get("c.eAction");

        action.setParams({
            "actionVal":    method.toString(),
            "objApiName":   objectName,
            "items":        component.get("v.listOfRecord")
        });
       
        action.setCallback(this, function(response) {
            var state = response.getState();    
            var resultsToast = $A.get("e.force:showToast");
            if (state === "SUCCESS") {
                var res = response.getReturnValue();                
                if(res != null){                    
                    resultsToast.setParams({
                        "type": "success",
                        "title": "Confirmation",
                        "message": component.get('v.messagesAction')[method.toString()]
                    });
                    component.set("v.listOfRecord", response.getReturnValue());  
                    $A.get('e.force:refreshView').fire();                        
                }
            }else{                
                var errors = response.getError();
                var message = 'Unknown error'; 
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                resultsToast.setParams({
                    "type":     "error",
                    "title":    "Review the errors on this page.",
                    "message":  message
                });  
            }
            resultsToast.fire();
        });            
        $A.enqueueAction(action);
    },

    /*Should define specific component which you want to add validation*/
    validateRequired: function(component, event) {        
        var isValid = true;
        if(component.get("v.componentName") == 'InlineEditLightningDataTableAddPRLI'){
            isValid = this.validateRequiredPRLI(component);
        } else if(component.get("v.componentName") == 'InlineEditLightningDataTableUpdatePRLI'){
            isValid = this.validateRequiredUpdatePRLI(component);
        } else if(component.get("v.componentName") == 'InlineEditLightningDataTableWOLI'){
            isValid = this.validateRequiredWOLI(component);
        }
        return isValid;
    },

    validateRequiredPRLI: function(component) {
        console.log('######validateRequiredPRLI');
        var isValid = true;
        var listNeedToBeUpdated = component.get("v.listOfNewRecord");      
        
        var errorMessage = 'These required fields must be completed.';
        for (var indexVar = 0; indexVar < listNeedToBeUpdated.length; indexVar++) {
            var errorInRow = 'Row '+ (indexVar + 1).toString() +': ';
            var isValidRow = true;
            console.log('###status: '+listNeedToBeUpdated[indexVar].Status__c);
           
            if (listNeedToBeUpdated[indexVar].Status__c == ""|| listNeedToBeUpdated[indexVar].Status__c == undefined) {                
                isValid = false;  
                isValidRow = false;  
                errorInRow+= 'Status, ';                         
            }
            if (listNeedToBeUpdated[indexVar].PRLI_Type__c == ""|| listNeedToBeUpdated[indexVar].PRLI_Type__c == undefined) {                
                isValid = false;  
                isValidRow = false;  
                errorInRow+= 'PRLI Type, ';                         
            }
            if (listNeedToBeUpdated[indexVar].Part_Description__c == "" || listNeedToBeUpdated[indexVar].Part_Description__c == undefined) {
                isValid = false;        
                isValidRow = false;  
                errorInRow+= 'Part Description, ';        
            }
            if (listNeedToBeUpdated[indexVar].Unit_of_Measure__c == "" || listNeedToBeUpdated[indexVar].Unit_of_Measure__c == undefined) {
                isValid = false;        
                isValidRow = false;   
                errorInRow+= 'Unit of Measure, ';        
            }
            if (listNeedToBeUpdated[indexVar].Quantity__c == "" || listNeedToBeUpdated[indexVar].Quantity__c == undefined) {                
                isValid = false;      
                isValidRow = false;  
                errorInRow+= 'Quantity';                                
            }            
        }

        if(!isValid){
            var resultsToast = $A.get("e.force:showToast");                
            resultsToast.setParams({
                "type":     "error",
                "title":    "Review the errors on this page.",
                "message":  errorMessage
            });  
            resultsToast.fire();            
        }
        return isValid;
    },

    // Validate Quality field on update PRLI
    validateRequiredUpdatePRLI: function(component) {
        console.log('######validateRequiredUpdatePRLI');
        var isValid = true;
        var listNeedToBeUpdated = component.get("v.listOfRecord");      
        
        var errorMessage = 'These required fields must be completed.';
        for (var indexVar = 0; indexVar < listNeedToBeUpdated.length; indexVar++) {
            var errorInRow = 'Row '+ (indexVar + 1).toString() +': ';
            var isValidRow = true;
            console.log('###status: '+listNeedToBeUpdated[indexVar].Status__c);
           
            if (listNeedToBeUpdated[indexVar].Status__c == ""  || listNeedToBeUpdated[indexVar].Status__c == undefined) {                
                isValid = false;  
                isValidRow = false;  
                errorInRow+= 'Status, ';                         
            }            
            if (listNeedToBeUpdated[indexVar].Quantity__c == "" || listNeedToBeUpdated[indexVar].Quantity__c == undefined) {                
                isValid = false;      
                isValidRow = false;  
                errorInRow+= 'Quantity';                                
            }   
        }

        if(!isValid){
            var resultsToast = $A.get("e.force:showToast");                
            resultsToast.setParams({
                "type":     "error",
                "title":    "Review the errors on this page.",
                "message":  errorMessage
            });  
            resultsToast.fire();            
        }
        return isValid;
    },

    // Validate Quality field on update PRLI
    validateRequiredWOLI: function(component) {
        console.log('######validateRequiredWOLI');
        var isValid = true;
        var listNeedToBeUpdated = component.get("v.listOfRecord");      
        
        var errorMessage = 'These required fields must be completed.';
        for (var indexVar = 0; indexVar < listNeedToBeUpdated.length; indexVar++) {
            var errorInRow = 'Row '+ (indexVar + 1).toString() +': ';
            var isValidRow = true;
            console.log('###status: '+listNeedToBeUpdated[indexVar].Status__c);
           
            if (listNeedToBeUpdated[indexVar].Status == "" || listNeedToBeUpdated[indexVar].Status == undefined) {                
                isValid = false;  
                isValidRow = false;  
                errorInRow+= 'Status, ';                         
            }  
        }

        if(!isValid){
            var resultsToast = $A.get("e.force:showToast");                
            resultsToast.setParams({
                "type":     "error",
                "title":    "Review the errors on this page.",
                "message":  errorMessage
            });  
            resultsToast.fire();            
        }
        return isValid;
    },
    getAllItemsList : function(component, event) {
        var objectName = component.get('v.objectName');
        var lstFldName = component.get('v.fieldNames');
        var lstFld = [];

        for (var fldNameNo in lstFldName) {
            var fldName = lstFldName[fldNameNo].split('+')[0];
            var fldType = lstFldName[fldNameNo].split('+')[1];
            if (fldType != undefined && fldType === "inputSelect") {
                lstFld.push(fldName);
            }
        }

        var action = component.get("c.getAllSelectOptions");
        action.setParams({
            "objName": objectName, 
            "lstFld": lstFld
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var optsMap = response.getReturnValue();
                console.log('@@@: ' + optsMap);
                component.set("v.picklistValueMap", JSON.stringify(response.getReturnValue()));
                this.getItemsList(component, event);
            }else{
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type":     "error",
                    "title":    "Error",
                    "message":  response.getError()
                });  
                resultsToast.fire();
            }            
        });
        $A.enqueueAction(action);
    },
})