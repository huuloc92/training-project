({     
    removeRow : function(component, event, helper){     
       component.getEvent("InlineEditLightningDeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    }, 
})