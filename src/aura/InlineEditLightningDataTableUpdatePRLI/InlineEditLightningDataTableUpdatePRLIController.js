({
    doInit : function(component, event, helper) {    	
    	var action = component.get("c.fetchUser");        
        action.setCallback(this, function(response) {
            var state = response.getState();            
            if (state === "SUCCESS") {
            	var user = JSON.stringify(response.getReturnValue());            	
                var userProfile = JSON.parse(user).Profile.Name;
                
                var actionMap = { submitAll: 'Submit All', receiveAll: 'Receive All', shipAll: 'Ship All'};
                
                if(userProfile == 'Retail' || userProfile == 'Retail Service Technician'){
                	actionMap = { submitAll: 'Submit All', receiveAll: 'Receive All'};
                }else if(userProfile == 'Home Building Facility' || userProfile == 'HBF Service Technician'){
                	actionMap = { submitAll: 'Submit All', shipAll: 'Ship All'};
                }               
                component.set('v.extraAction', actionMap);
                component.set('v.initUserData', true);                
            }         
        });
        $A.enqueueAction(action); 
    }
})